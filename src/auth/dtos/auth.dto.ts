export class JwtPayloadDto {
  username: string;
  sub: {
    id: string;
  };
}

export class UserJwtPayloadDto {
  id: number;
  username: string;
}
