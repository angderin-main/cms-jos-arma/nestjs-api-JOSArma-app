import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { UserModel } from './models/user.model';

import * as bcrypt from 'bcrypt';
import { AuthConfig } from 'src/constants/auth.config';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(UserModel)
    private userModel: typeof UserModel,
  ) {}

  async findOne(username: string): Promise<UserModel> {
    return this.userModel.findOne({
      where: {
        username: username,
      },
    });
  }

  async findAll() {
    return this.userModel.findAll();
  }

  async createOne(username: string, password: string) {
    const hashPassword = await bcrypt.hash(password, AuthConfig.salt);
    return this.userModel.create({
      username: username,
      password: hashPassword,
    });
  }
}
