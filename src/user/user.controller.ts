import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateUserDto, FindUserDto, LoginUserDto } from './dtos/user.dto';
import { UserService } from './user.service';

import * as bcrypt from 'bcrypt';
import { Public } from 'src/auth/decorators/public.decorator';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('signin')
  async loginHandler(@Body() findUserDto: LoginUserDto) {
    const user = await this.userService.findOne(findUserDto.username);

    if (!user || !(await bcrypt.compare(findUserDto.password, user.password)))
      return {
        data: null,
      };

    return {
      data: user,
    };
  }

  @Public()
  @Post('signup')
  async signupHandler(@Body() createUserDto: CreateUserDto) {
    const newUser = await this.userService.createOne(
      createUserDto.username,
      createUserDto.password,
    );

    return {
      data: newUser,
    };
  }

  @Get()
  async getUserHandler() {
    const users = await this.userService.findAll();
    return {
      data: users,
    };
  }
}
