export class CreateUserDto {
  username: string;
  password: string;
}

export class FindUserDto {
  username: string;
}

export class LoginUserDto {
  username: string;
  password: string;
}